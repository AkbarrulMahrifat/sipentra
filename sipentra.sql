-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 21, 2018 at 08:24 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sipentra`
--

-- --------------------------------------------------------

--
-- Table structure for table `bentuk_kegiatan`
--

CREATE TABLE `bentuk_kegiatan` (
  `id_bentuk` int(11) NOT NULL,
  `nama_bentuk` varchar(50) NOT NULL,
  `nilai_bentuk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bentuk_kegiatan`
--

INSERT INTO `bentuk_kegiatan` (`id_bentuk`, `nama_bentuk`, `nilai_bentuk`) VALUES
(1, 'Seminar', 0),
(2, 'Talkshow', 0),
(3, 'Diskusi Panel', 0),
(4, 'Simposium', 0),
(5, 'Konferensi', 0),
(6, 'Kuliah Umum', 0),
(7, 'Penulisan Karya Ilmiah', 10),
(8, 'Publikasi Ilmiah', 10),
(9, 'Olimpiade IPTEKS & Olahraga', 0),
(10, 'Bakti Sosial', 0),
(11, 'Produksi', 0),
(12, 'Studi Banding', 0),
(13, 'Asisten Penelitian Dosen', 0),
(14, 'Asisten Laboratorium', 10),
(15, 'Pelatihan', 0),
(16, 'Workshop', 0),
(17, 'Pameran', 0),
(18, 'Pagelaran', 0),
(19, 'Lomba', 0),
(20, 'Pengurus Ormawa', 10),
(21, 'Membentuk Start Up', 10),
(22, 'Kegiatan Insidental', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bidang_kegiatan`
--

CREATE TABLE `bidang_kegiatan` (
  `id_bidang` int(11) NOT NULL,
  `nama_bidang` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bidang_kegiatan`
--

INSERT INTO `bidang_kegiatan` (`id_bidang`, `nama_bidang`) VALUES
(1, 'Penalaran'),
(2, 'Minat Bakat'),
(3, 'Pengabdian Masyarakat'),
(4, 'Kegiatan Kesejahteraan Mahasiswa');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan_kegiatan`
--

CREATE TABLE `jabatan_kegiatan` (
  `id_jabatan` int(11) NOT NULL,
  `nama_jabatan` varchar(50) NOT NULL,
  `nilai_jabatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan_kegiatan`
--

INSERT INTO `jabatan_kegiatan` (`id_jabatan`, `nama_jabatan`, `nilai_jabatan`) VALUES
(1, 'Ketua', 50),
(2, 'Anggota', 40),
(3, 'Ketua Panitia', 30),
(4, 'Anggota Panitia', 25),
(5, 'Penampil / Talent / Guide', 20),
(6, 'Peserta', 30),
(7, 'Pemateri', 25);

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id_kegiatan` int(11) NOT NULL,
  `nama_kegiatan` varchar(50) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `id_bentuk` int(11) NOT NULL,
  `id_ukuran` int(11) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  `id_ormawa` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `proposal` varchar(50) DEFAULT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`id_kegiatan`, `nama_kegiatan`, `tgl_mulai`, `tgl_selesai`, `id_bentuk`, `id_ukuran`, `id_bidang`, `id_ormawa`, `deskripsi`, `proposal`, `is_delete`) VALUES
(1, 'Pengurus Periode 2017/2018', '2018-10-24', '2018-12-08', 20, 2, 1, 'ukmketalase', 'Kepengurusan UKMK Etalase Periode 2017/2018', '', 0),
(2, 'Pekan Seni (EkSen) 2018', '2018-10-10', '2018-12-08', 18, 5, 1, 'ukmketalase', 'Pagelaran Seni UKMK Etalase', '', 0),
(3, 'Pameran Fotografi', '2018-10-26', '0000-00-00', 17, 4, 1, 'ukmketalase', 'Pameran Fotografi UKMK Etalase', NULL, 0),
(4, 'Pementasan Teater Tunggal', '2018-10-26', '0000-00-00', 18, 5, 1, 'ukmketalase', 'Pementasan Teater Tunggal', 'materi_keorganisasian.docx', 0),
(5, 'Pameran Fotografi 2018', '2018-10-26', '0000-00-00', 17, 5, 1, 'ukmketalase', 'Pameran Fotografi', 'Surat_Peminjaman_Tempat_1.pdf', 0),
(6, 'A', '2018-12-08', '2018-12-23', 10, 2, 3, 'ukmketalase', 'Bakti Sosial', 'nota.pdf', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mahasiswa` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nim` varchar(50) NOT NULL,
  `prodi` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id_mahasiswa`, `nama`, `nim`, `prodi`, `username`) VALUES
(1, 'Akbarrul Mahrifat', '142410101024', 'Sistem Informasi', '142410101024'),
(2, 'Fathoni Wahyudi', '142410101024', 'Sistem Informasi', '142410101025');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_ekstra`
--

CREATE TABLE `nilai_ekstra` (
  `id_nilai` int(11) NOT NULL,
  `id_mahasiswa` varchar(50) NOT NULL,
  `id_ormawa` varchar(50) NOT NULL,
  `nama_kegiatan` varchar(50) NOT NULL,
  `bentuk_kegiatan` varchar(50) NOT NULL,
  `ukuran_kegiatan` varchar(50) NOT NULL,
  `bidang_kegiatan` varchar(50) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `total_nilai` int(11) NOT NULL,
  `label_nilai` varchar(10) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_ekstra`
--

INSERT INTO `nilai_ekstra` (`id_nilai`, `id_mahasiswa`, `id_ormawa`, `nama_kegiatan`, `bentuk_kegiatan`, `ukuran_kegiatan`, `bidang_kegiatan`, `tgl_mulai`, `tgl_selesai`, `jabatan`, `total_nilai`, `label_nilai`, `status`) VALUES
(5, '142410101024', 'ukmketalase', 'Pameran Fotografi', 'Pameran', 'Regional Kota/Kabupaten', 'Penalaran', '2018-10-26', '0000-00-00', 'Ketua Panitia', 55, 'ORMAWA', 1),
(6, '142410101024', 'ukmketalase', 'Pementasan Teater Tunggal', 'Pagelaran', 'Regional Tapal Kuda', 'Penalaran', '2018-10-26', '0000-00-00', 'Anggota Panitia', 55, 'ORMAWA', 2),
(7, '142410101024', 'ukmketalase', 'AAA', 'Seminar', 'Program Studi', 'Penalaran', '2018-12-18', '2018-12-18', 'Peserta', 40, 'MANDIRI', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ormawa`
--

CREATE TABLE `ormawa` (
  `id_ormawa` int(11) NOT NULL,
  `nama_ormawa` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ormawa`
--

INSERT INTO `ormawa` (`id_ormawa`, `nama_ormawa`, `username`) VALUES
(1, 'Badan Eksekutif Mahasiswa', 'bem'),
(2, 'Badan Perwakilan Mahasiswa', 'bpm'),
(3, 'Himpunan Mahasiswa Sistem Informasi', 'himasif'),
(4, 'Himpunan Mahasiswa Teknologi Informasi', 'himatif'),
(5, 'Himpunan Mahasiswa Informatika', 'himatik'),
(6, 'UKMK Etalase', 'ukmketalase'),
(7, 'Mapala Balwana', 'mapalabalwana'),
(8, 'UKMO MACO', 'ukmomaco'),
(9, 'UKMP Binary', 'ukmpbinary'),
(10, 'UKM LAOS', 'ukmlaos');

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan_nilai_ekstra`
--

CREATE TABLE `pengajuan_nilai_ekstra` (
  `id_nilai` int(11) NOT NULL,
  `id_mahasiswa` varchar(50) NOT NULL,
  `nama_kegiatan` varchar(225) NOT NULL,
  `id_bentuk` int(11) NOT NULL,
  `id_ukuran` int(11) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  `id_ormawa` varchar(50) NOT NULL,
  `jabatan` int(11) NOT NULL,
  `total_nilai` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajuan_nilai_ekstra`
--

INSERT INTO `pengajuan_nilai_ekstra` (`id_nilai`, `id_mahasiswa`, `nama_kegiatan`, `id_bentuk`, `id_ukuran`, `id_bidang`, `id_ormawa`, `jabatan`, `total_nilai`, `status`) VALUES
(1, '142410101024', 'Aaaaa', 1, 1, 1, 'ukmketalase', 4, 50, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ukuran_kegiatan`
--

CREATE TABLE `ukuran_kegiatan` (
  `id_ukuran` int(11) NOT NULL,
  `nama_ukuran` varchar(50) NOT NULL,
  `nilai_ukuran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ukuran_kegiatan`
--

INSERT INTO `ukuran_kegiatan` (`id_ukuran`, `nama_ukuran`, `nilai_ukuran`) VALUES
(1, 'Program Studi', 10),
(2, 'Fakultas', 15),
(3, 'Universitas', 20),
(4, 'Regional Kota/Kabupaten', 25),
(5, 'Regional Tapal Kuda', 30),
(6, 'Propinsi', 35),
(7, 'Nasional', 40);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `nama`, `role`) VALUES
('123456789', 'kemahasiswaan', 'Kemahasiswaan', 2),
('142410101024', 'mahasiswa', 'Akbarrul Mahrifat', 3),
('142410101025', 'mahasiswa', 'Fathoni Wahyudi', 3),
('bem', 'ormawa', 'Badan Eksekutif Mahasiswa', 1),
('bpm', 'ormawa', 'Badan Perwakilan Mahasiswa', 1),
('himasif', 'ormawa', 'Himpunan Mahasiswa Sistem Informasi', 1),
('himatif', 'ormawa', 'Himpunan Mahasiswa Teknologi Informasi', 1),
('himatik', 'ormawa', 'Himpunan Mahasiswa Informatika', 1),
('mapalabalwana', 'ormawa', 'MAPALA Balwana', 1),
('ukmketalase', 'ormawa', 'UKMK Etalase', 1),
('ukmlaos', 'ormawa', 'UKM LAOS', 1),
('ukmomaco', 'ormawa', 'UKMO MACO', 1),
('ukmpbinary', 'ormawa', 'UKMP Binary', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bentuk_kegiatan`
--
ALTER TABLE `bentuk_kegiatan`
  ADD PRIMARY KEY (`id_bentuk`);

--
-- Indexes for table `bidang_kegiatan`
--
ALTER TABLE `bidang_kegiatan`
  ADD PRIMARY KEY (`id_bidang`);

--
-- Indexes for table `jabatan_kegiatan`
--
ALTER TABLE `jabatan_kegiatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`),
  ADD KEY `id_bentuk` (`id_bentuk`),
  ADD KEY `id_ukuran` (`id_ukuran`),
  ADD KEY `id_ormawa` (`id_ormawa`),
  ADD KEY `id_bidang` (`id_bidang`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id_mahasiswa`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `nilai_ekstra`
--
ALTER TABLE `nilai_ekstra`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `id_mahasiswa` (`id_mahasiswa`),
  ADD KEY `id_kegiatan` (`nama_kegiatan`),
  ADD KEY `jabatan` (`jabatan`);

--
-- Indexes for table `ormawa`
--
ALTER TABLE `ormawa`
  ADD PRIMARY KEY (`id_ormawa`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `pengajuan_nilai_ekstra`
--
ALTER TABLE `pengajuan_nilai_ekstra`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `jabatan` (`jabatan`),
  ADD KEY `id_ormawa` (`id_ormawa`),
  ADD KEY `id_mahasiswa` (`id_mahasiswa`),
  ADD KEY `id_bentuk` (`id_bentuk`),
  ADD KEY `id_ukuran` (`id_ukuran`),
  ADD KEY `id_bidang` (`id_bidang`);

--
-- Indexes for table `ukuran_kegiatan`
--
ALTER TABLE `ukuran_kegiatan`
  ADD PRIMARY KEY (`id_ukuran`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bentuk_kegiatan`
--
ALTER TABLE `bentuk_kegiatan`
  MODIFY `id_bentuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `bidang_kegiatan`
--
ALTER TABLE `bidang_kegiatan`
  MODIFY `id_bidang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jabatan_kegiatan`
--
ALTER TABLE `jabatan_kegiatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id_mahasiswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `nilai_ekstra`
--
ALTER TABLE `nilai_ekstra`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ormawa`
--
ALTER TABLE `ormawa`
  MODIFY `id_ormawa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pengajuan_nilai_ekstra`
--
ALTER TABLE `pengajuan_nilai_ekstra`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ukuran_kegiatan`
--
ALTER TABLE `ukuran_kegiatan`
  MODIFY `id_ukuran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD CONSTRAINT `kegiatan_ibfk_2` FOREIGN KEY (`id_bentuk`) REFERENCES `bentuk_kegiatan` (`id_bentuk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kegiatan_ibfk_3` FOREIGN KEY (`id_ukuran`) REFERENCES `ukuran_kegiatan` (`id_ukuran`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kegiatan_ibfk_4` FOREIGN KEY (`id_ormawa`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kegiatan_ibfk_5` FOREIGN KEY (`id_bidang`) REFERENCES `bidang_kegiatan` (`id_bidang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nilai_ekstra`
--
ALTER TABLE `nilai_ekstra`
  ADD CONSTRAINT `nilai_ekstra_ibfk_4` FOREIGN KEY (`id_mahasiswa`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ormawa`
--
ALTER TABLE `ormawa`
  ADD CONSTRAINT `ormawa_ibfk_1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengajuan_nilai_ekstra`
--
ALTER TABLE `pengajuan_nilai_ekstra`
  ADD CONSTRAINT `pengajuan_nilai_ekstra_ibfk_1` FOREIGN KEY (`id_mahasiswa`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengajuan_nilai_ekstra_ibfk_2` FOREIGN KEY (`jabatan`) REFERENCES `jabatan_kegiatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
