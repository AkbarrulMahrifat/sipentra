<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Detail Kegiatan Ekstra <?=$detail['id_kegiatan']?>
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>ID Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detail['id_kegiatan']?> </td>
                        </tr>
                        <tr>
                            <td>Nama Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detail['nama_kegiatan']?> </td>
                        </tr>
                        <tr>
                            <td>Tanggal Mulai</td>
                            <td> : </td>
                            <td> <?=$detail['tgl_mulai']?> </td>
                        </tr>
                        <tr>
                            <td>Tanggal Selesai</td>
                            <td> : </td>
                            <td> <?=$detail['tgl_selesai']?> </td>
                        </tr>
                        <tr>
                            <td>Bentuk Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detail['nama_bentuk']?> </td>
                        </tr>
                        <tr>
                            <td>Ukuran Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detail['nama_ukuran']?> </td>
                        </tr>
                        <tr>
                            <td>Bidang Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detail['nama_bidang']?> </td>
                        </tr>
                        <tr>
                            <td>Deskripsi Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detail['deskripsi']?> </td>
                        </tr>
                        <tr>
                            <td>Proposal Kegiatan</td>
                            <td> : </td>
                            <td>
                                <?php if ($detail['proposal'] != NULL){ ?>
                                <?=$detail['proposal']?>
                                <a href="<?=site_url('Kegiatan/downloadproposal/'.$detail['id_kegiatan'])?>" class="btn btn-xs btn-default"><i class="material-icons">file_download</i></a>
                                <?php } else{ echo "Tidak ada proposal yang diupload."; }?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <a class="btn btn-sm btn-primary" href="<?=site_url('Kegiatan')?>"><i class="material-icons">arrow_back</i><span>Kembali</span></a>
            </div>
        </div>
    </div>
</div>