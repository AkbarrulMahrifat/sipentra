<div class="col-sm-12">
    <div class="card">
        <div class="header">
            <h2>
                Tambah Data Kegiatan Ekstra
            </h2>
        </div>
        <div class="body">
            <?php echo form_open_multipart('Kegiatan/tambahkegiatan'); ?>

            <div class="row clearfix">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Nama Kegiatan</label>
                            <input name="nama" type="text" class="form-control" placeholder="Nama Kegiatan" required>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Tanggal Mulai</label>
                            <input name="tanggal_mulai" type="text" class="form-control datepicker" placeholder="Tanggal Kegiatan" required>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Tanggal Selesai</label>
                            <input name="tanggal_selesai" type="text" class="form-control datepicker" placeholder="Tanggal Kegiatan" required>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-line">
                            <label for="bentuk_kegiatan">Bentuk Kegiatan</label>
                            <select name="bentuk" class="form-control" data-live-search="true" required>
                                <option value="">-- Bentuk Kegiatan --</option>
                                <?php foreach ($bentuk as $b) { ?>
                                    <option value="<?=$b->id_bentuk?>"><?=$b->nama_bentuk?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-line">
                            <label for="ukuran_kegiatan">Ukuran Kegiatan</label>
                            <select name="ukuran" class="form-control" data-live-search="true" required>
                                <option value="">-- Ukuran Kegiatan --</option>
                                <?php foreach ($ukuran as $u) { ?>
                                    <option value="<?=$u->id_ukuran?>"><?=$u->nama_ukuran?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-line">
                            <label for="ukuran_kegiatan">Bidang Kegiatan</label>
                            <select name="bidang" class="form-control" data-live-search="true" required>
                                <option value="">-- Bidang Kegiatan --</option>
                                <?php foreach ($bidang as $b) { ?>
                                    <option value="<?=$b->id_bidang?>"><?=$b->nama_bidang?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-line">
                            <label for="deskripsi_kegiatan">Deskripsi Kegiatan</label>
                            <textarea name="deskripsi" type="text" class="form-control" placeholder="Deskripsi Kegiatan" required></textarea>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-line">
                            <label for="proposal_kegiatan">Proposal Kegiatan</label>
                            <input name="proposal" type="file" class="form-control" placeholder="Proposal Kegiatan" required>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success m-t-15 waves-effect"><i class="material-icons">save</i><span>SIMPAN</span></button>

            </form>
        </div>
    </div>
</div>
