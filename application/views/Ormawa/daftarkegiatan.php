<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Daftar Kegiatan Ekstra <?=$this->session->userdata('nama')?>
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama Kegiatan</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Bentuk</th>
                            <th>Ukuran</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($kegiatan as $k) { ?>
                        <tr>
                            <td><?=$k->id_kegiatan?></td>
                            <td><?=$k->nama_kegiatan?></td>
                            <td><?=$k->tgl_mulai?></td>
                            <td><?=$k->tgl_selesai?></td>
                            <td><?=$k->nama_bentuk?></td>
                            <td><?=$k->nama_ukuran?></td>
                            <td style="min-width: 80px; text-align: center">
                                <a class="btn btn-xs btn-primary" href="<?=site_url('Kegiatan/detailkegiatan/'.$k->id_kegiatan)?>"><i class="material-icons">search</i></a>
                                <a class="btn btn-xs btn-info editkegiatan" data-idkegiatan="<?=$k->id_kegiatan?>" data-namakegiatan="<?=$k->nama_kegiatan?>" data-tglmulai="<?=$k->tgl_mulai?>" data-tglselesai="<?=$k->tgl_selesai?>" data-bentukkegiatan="<?=$k->id_bentuk?>" data-ukurankegiatan="<?=$k->id_ukuran?>" data-bidangkegiatan="<?=$k->id_bidang?>" data-deskripsikegiatan="<?=$k->deskripsi?>"  data-oldproposal="<?=$k->proposal?>"><i class="material-icons">edit</i></a>
                                <a class="btn btn-xs btn-danger hapus" href="<?=site_url('Kegiatan/deletekegiatan/'.$k->id_kegiatan)?>" data-confirm="Apakah anda yakin ingin menghapus data ini ?"><i class="material-icons">delete</i></a>
                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Modal Edit Kegiatan-->
<div class="modal fade" id="editkegiatan" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Edit Data Kegiatan Ekstra</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('Kegiatan/updatekegiatan'); ?>
                <input type="hidden" id="id_kegiatan" name="id">
                    <div class="row clearfix">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <label>Nama Kegiatan</label>
                                    <input name="nama" type="text" class="form-control" placeholder="Nama Kegiatan" id="nama_kegiatan" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <label>Tanggal Mulai</label>
                                    <input name="tanggal_mulai" type="text" class="form-control datepicker" placeholder="Tanggal Mulai" id="tgl_mulai" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <label>Tanggal Selesai</label>
                                    <input name="tanggal_selesai" type="text" class="form-control datepicker" placeholder="Tanggal Selesai" id="tgl_selesai" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="bentuk_kegiatan">Bentuk Kegiatan</label>
                                    <select name="bentuk" class="form-control" id="bentuk_kegiatan" data-live-search="true" required>
                                        <option value="">-- Bentuk Kegiatan --</option>
                                        <?php foreach ($bentuk as $b) { ?>
                                            <option value="<?=$b->id_bentuk?>"><?=$b->nama_bentuk?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="ukuran_kegiatan">Ukuran Kegiatan</label>
                                    <select name="ukuran" class="form-control" id="ukuran_kegiatan" data-live-search="true" required>
                                        <option value="">-- Ukuran Kegiatan --</option>
                                        <?php foreach ($ukuran as $u) { ?>
                                            <option value="<?=$u->id_ukuran?>"><?=$u->nama_ukuran?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="bidang_kegiatan">Bidang Kegiatan</label>
                                    <select name="bidang" class="form-control" id="bidang_kegiatan" data-live-search="true" required>
                                        <option value="">-- Bidang Kegiatan --</option>
                                        <?php foreach ($bidang as $b) { ?>
                                            <option value="<?=$b->id_bidang?>"><?=$b->nama_bidang?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="deskripsi_kegiatan">Deskripsi Kegiatan</label>
                                    <textarea name="deskripsi" type="text" class="form-control" placeholder="Deskripsi Kegiatan" id="deskripsi_kegiatan" required></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="proposal_kegiatan">Proposal Kegiatan</label>
                                    <input name="old_proposal" type="text" id="old_proposal" class="form-control" readonly>
                                    <input name="proposal" type="file" class="form-control" placeholder="Proposal Kegiatan">
                                </div>
                            </div>
                        </div>

                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-link waves-effect">SIMPAN</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
            </div>
            </form>
        </div>
    </div>
</div>