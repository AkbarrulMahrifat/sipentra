<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Validasi Nilai Ekstra
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>Nama Mahasiswa</th>
                            <th>NIM</th>
                            <th>Nama Kegiatan</th>
                            <th>Jabatan</th>
                            <th>Status</th>
                            <th>Validasi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($nilai as $n) { ?>
                            <tr>
                                <td><?=$n->nama?></td>
                                <td><?=$n->username?></td>
                                <td><?=$n->nama_kegiatan?></td>
                                <td><?=$n->jabatan?></td>
                                <td style="text-align: center">
                                    <?php if ($n->status == 0){?>
                                        <span class="label bg-blue">Belum Disetujui</span>
                                    <?php } ?>
                                    <?php if ($n->status == 1){?>
                                        <span class="label bg-green">Telah Disetujui</span>
                                    <?php } ?>
                                    <?php if ($n->status == 2){?>
                                        <span class="label bg-red">Ditolak</span>
                                    <?php } ?>
                                </td>
                                <td style="min-width: 80px; text-align: center">
                                    <?php if ($n->status != 1){?>
                                        <a class="btn btn-xs btn-success validasi" href="<?=site_url('Nilai/validasinilai/'.$n->id_nilai."/1")?>" data-confirm="Apakah anda yakin ingin menyetujui pengajuan nilai ekstra ini ?"><i class="material-icons">check</i></a>
                                    <?php } ?>

                                    <?php if ($n->status != 2){?>
                                        <a class="btn btn-xs btn-danger validasi" href="<?=site_url('Nilai/validasinilai/'.$n->id_nilai."/2")?>" data-confirm="Apakah anda yakin ingin menolak pengajuan nilai ekstra ini ?"><i class="material-icons">clear</i></a>
                                    <?php } ?>

                                    <?php if ($n->status != 0){?>
                                        <a class="btn btn-xs btn-primary validasi" href="<?=site_url('Nilai/validasinilai/'.$n->id_nilai."/0")?>" data-confirm="Apakah anda yakin ingin membatalkan validasi ini ?"><i class="material-icons">remove</i></a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>