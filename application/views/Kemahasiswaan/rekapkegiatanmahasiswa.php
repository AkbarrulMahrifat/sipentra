<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Daftar Mahasiswa Kegiatan Ekstra
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIM</th>
                            <th>Nilai</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; foreach ($mahasiswa as $m) { ?>
                            <tr>
                                <td><?=$no?></td>
                                <td><?=$m->nama?></td>
                                <td><?=$m->username?></td>
                                <td><?=$m->total_nilai?></td>
                            </tr>
                        <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>