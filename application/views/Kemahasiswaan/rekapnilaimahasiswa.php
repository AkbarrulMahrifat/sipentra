<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Rekap Nilai Ekstra
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table>
                        <tbody>
                        <tr>
                            <td style="min-width: 100px">Nama</td>
                            <td style="min-width: 30px"> : </td>
                            <td> <?=$mahasiswa['nama']?> </td>
                        </tr>
                        <tr>
                            <td style="min-width: 100px">NIM</td>
                            <td style="min-width: 30px"> : </td>
                            <td> <?=$mahasiswa['nim']?> </td>
                        </tr>
                        <tr>
                            <td style="min-width: 100px">Program Studi</td>
                            <td style="min-width: 30px"> : </td>
                            <td> <?=$mahasiswa['prodi']?> </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                        <tr>
                            <th style="width: 5px">No</th>
                            <th>Nama Kegiatan</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Jabatan</th>
                            <th>Nilai</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $totalnilai = 0; $no=1; foreach ($nilai as $n) { ?>
                            <tr>
                                <td><?=$no?></td>
                                <td><?=$n->nama_kegiatan?></td>
                                <td><?=$n->tgl_mulai?></td>
                                <td><?=$n->tgl_selesai?></td>
                                <td><?=$n->jabatan?></td>
                                <td><?=$n->total_nilai?></td>
                            </tr>
                            <?php $total = $totalnilai += $n->total_nilai; $no++; } ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <?php if ($nilai != NULL){ ?>
                                <td colspan="5" class="text-center">Total Nilai</td>
                                <td>
                                    <?php
                                    echo $total;
                                    ?>
                                </td>
                            <?php } else { ?>
                            <td colspan="5" class="text-center">Mahasiswa Ini Belum Memiliki Nilai Ekstra Yang Telah Disetujui.</td>
                            <?php } ?>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <br>
                <a class="btn btn-primary" href="<?=site_url('Nilai')?>"><i class="material-icons">arrow_back</i><span>Kembali</span></a>
            </div>
        </div>
    </div>
</div>