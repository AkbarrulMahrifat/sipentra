<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Daftar Ormawa
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Ormawa</th>
                            <th>Jumlah Kegiatan</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; foreach ($ormawa as $o) { ?>
                            <tr>
                                <td><?=$no?></td>
                                <td><?=$o->nama?></td>
                                <td>
                                    <?php
                                    $this->db->select('*');
                                    $this->db->from('kegiatan');
                                    $this->db->where('id_ormawa', $o->username);
                                    $this->db->where('is_delete', 0);
                                    $jumlahkegiatan = $this->db->count_all_results();
                                    echo $jumlahkegiatan;
                                    ?>
                                </td>
                                <td style="min-width: 80px; text-align: center">
                                    <a class="btn btn-xs btn-primary" href="<?=site_url('Kegiatan/rekapkegiatan/'.$o->username)?>"><i class="material-icons">search</i></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>