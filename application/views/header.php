<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>SIPENTRA - <?=$this->uri->segment(1) ?></title>
    <!-- Favicon-->
    <link rel="icon" href="<?=base_url()?>assets/assetsLanding/img/unej.png" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?=base_url()?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?=base_url()?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?=base_url()?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="<?=base_url()?>assets/plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?=base_url()?>assets/css/themes/all-themes.css" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?=base_url()?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="<?=base_url()?>assets/plugins/waitme/waitMe.css" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="<?=base_url()?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>

    <!-- Jquery DataTable Plugin CSS -->
    <link href="<?=base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
</head>

<body class="theme-red">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Search Bar -->
<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="START TYPING...">
    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>
<!-- #END# Search Bar -->
<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="<?=base_url();?>">SIPENTRA - Sistem Informasi Penilaian Ekstra</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->

                <!-- #END# Notifications -->

            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="<?=base_url()?>assets/assetsLanding/img/unej.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=$this->session->userdata('nama')?></div>
                <div class="email"><?=$this->session->userdata('username')?></div>
<!--                <div class="btn-group user-helper-dropdown">-->
<!--                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>-->
<!--                    <ul class="dropdown-menu pull-right">-->
<!--                        <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>-->
<!--                        <li role="separator" class="divider"></li>-->
<!--                        <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>-->
<!--                        <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>-->
<!--                        <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>-->
<!--                        <li role="separator" class="divider"></li>-->
<!--                        <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>-->
<!--                    </ul>-->
<!--                </div>-->
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li class="<?php if ($this->uri->segment(1)== "Beranda"){echo "active";}?>">
                    <a href="<?=site_url('Beranda')?>">
                        <i class="material-icons">home</i>
                        <span>Beranda</span>
                    </a>
                </li>

                <li class="<?php if ($this->uri->segment(1)== "Kegiatan"){echo "active";}?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">view_list</i>
                        <span>Kegiatan Ekstra</span>
                    </a>
                    <ul class="ml-menu">
                        <?php if ($this->session->userdata('role') != 1) { ?>
                            <li class="<?php if ($this->uri->segment(1)== "Kegiatan"){echo "active";}?>">
                                <a href="<?=site_url('Kegiatan')?>">Daftar Kegiatan Ekstra</a>
                            </li>
                        <?php } ?>

                        <?php if ($this->session->userdata('role') == 1) { ?>
                            <li class="<?php if ($this->uri->segment(1)== "Kegiatan" && $this->uri->segment(2)== NULL){echo "active";}?>">
                                <a href="<?=site_url('Kegiatan')?>">Daftar Kegiatan Ekstra</a>
                            </li>
                            <li class="<?php if ($this->uri->segment(1)== "Kegiatan" && $this->uri->segment(2)== 'inputkegiatan'){echo "active";}?>">
                                <a href="<?=site_url('Kegiatan/inputkegiatan')?>">Tambah Kegiatan</a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>

                <li class="<?php if ($this->uri->segment(1)== "Nilai"){echo "active";}?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">assignment</i>
                        <span>Nilai Ekstra</span>
                    </a>
                    <ul class="ml-menu">
                        <?php if ($this->session->userdata('role') == 1) { ?>
                            <li class="<?php if ($this->uri->segment(1)== "Nilai" && $this->uri->segment(2)== NULL){echo "active";}?>">
                                <a href="<?=site_url('Nilai')?>">Validasi Nilai Ekstra</a>
                            </li>
                        <?php } ?>

                        <?php if ($this->session->userdata('role') == 2) { ?>
                            <li class="<?php if ($this->uri->segment(1)== "Nilai"){echo "active";}?>">
                                <a href="<?=site_url('Nilai')?>">Daftar Nilai Ekstra</a>
                            </li>
                        <?php } ?>

                        <?php if ($this->session->userdata('role') == 3) { ?>
                            <li class="<?php if ($this->uri->segment(1)== "Nilai" && $this->uri->segment(2)== NULL){echo "active";}?>">
                                <a href="<?=site_url('Nilai')?>">Daftar Nilai Ekstra</a>
                            </li>
                            <li class="<?php if ($this->uri->segment(1)== "Nilai" && $this->uri->segment(2)== 'pengajuannilaiekstra'){echo "active";}?>">
                                <a href="<?=site_url('Nilai/pengajuannilaiekstra')?>">Pengajuan Nilai Ekstra</a>
                            </li>
                            <li class="<?php if ($this->uri->segment(1)== "Nilai" && $this->uri->segment(2)== 'rekapnilaiekstra'){echo "active";}?>">
                                <a href="<?=site_url('Nilai/rekapnilaiekstra')?>">Rekap Nilai Ekstra</a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>

                <li class="header">USER</li>
                <li>
                    <a href="<?=site_url('Landing/logout')?>">
                        <i class="material-icons">power_settings_new</i>
                        <span>Logout</span>
                    </a>
                </li>

            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2016 - 2017 <a href="javascript:void(0);">SIPENTRA - F. ILKOM</a>.
            </div>
            <div class="version">
                <b>Version: </b> 1.0.5
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->

</section>

<!--Awal Halaman-->
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 class="text-uppercase"><?=$this->uri->segment(1)?> Ekstra</h2>
        </div>
        <?php if ($message = $this->session->flashdata('success')) { ?>
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong><?=$message?></strong>
        </div>
        <?php } ?>

        <?php if ($message = $this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong><?=$message?></strong>
        </div>
        <?php } ?>
