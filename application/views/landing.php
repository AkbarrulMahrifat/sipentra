<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>WELCOME TO SIPENTRA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="<?=base_url()?>assets/assetsLanding/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/assetsLanding/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/assetsLanding/color/default.css" rel="stylesheet">
    <link rel="shortcut icon" href="<?=base_url()?>assets/assetsLanding/img/unej.png">
    <!-- =======================================================
    Theme Name: Maxim
    Theme URL: https://bootstrapmade.com/maxim-free-onepage-bootstrap-theme/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
    ======================================================= -->
</head>

<body>
<!-- navbar -->
<div class="navbar-wrapper">
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <!-- Responsive navbar -->
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </a>
                <h1 class="brand"><a href="<?=base_url()?>">SIPENTRA</a></h1>
                <!-- navigation -->
                <nav class="pull-right nav-collapse collapse">
                    <ul id="menu-main" class="nav">
                        <li><a title="aktor" href="#about">Tentang SIPENTRA</a></li>
                        <li><a title="alur" href="#services">Alur SIPENTRA</a></li>
                        <li><a title="login" href="javascript:;" data-toggle="modal" data-target=".bs-example-modal-lg">Login</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>

<!-- Header area -->
<div id="header-wrapper" class="header-slider">
    <header class="clearfix">
        <div class="logo">
            <img src="<?=base_url()?>assets/assetsLanding/img/unej.png" alt="" />
        </div>
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div id="main-flexslider" class="flexslider">
                        <ul class="slides">
                            <li>
                                <p class="home-slide-content">
                                    <strong>Fakultas</strong> Ilmu Komputer
                                </p>
                            </li>
                            <li>
                                <p class="home-slide-content">
                                    PS. <strong>Sistem Informasi</strong>
                                </p>
                            </li>
                            <li>
                                <p class="home-slide-content">
                                    Welcome to <strong>SIPENTRA</strong>
                                </p>
                            </li>
                        </ul>
                    </div>
                    <!-- end slider -->
                </div>
            </div>
        </div>
    </header>
</div>

<!-- section: team -->
<section id="about" class="section">
    <div class="container">
        <h4>Apa itu SIPENTRA ?</h4>
        <div class="row">
            <div class="span4 offset1">
                <div>
                    <h2>Sistem Informasi Penilaian Ekstra</h2>
                    <p>
                        SIPENTRA merupakan sistem informasi yang mengelola penilaian ekstra dari mahasiswa Program Ilmu Komputer. Dengan adanya sistem informasi ini mahasiswa yang aktif dalam kegiatan organisasi di kampus dapat melihat nilai ekstra yang didapat.
                    </p>
                </div>
            </div>
            <div class="span6">
                <div class="aligncenter">
                    <img src="<?=base_url()?>assets/assetsLanding/img/icons/creativity.png" alt="" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="span3 flyIn">
                <div class="people">
                    <img class="team-thumb img-circle" src="<?=base_url()?>assets/assetsLanding/img/team/img-2.jpg" alt="" />
                    <h3>ORMAWA</h3>
                    <p>
                        Admin dari tiap ORMAWA (Organisasi Mahasiswa) di ILKOM yang dapat mengelola kegiatan ekstra
                    </p>
                </div>
            </div>
            <div class="span1"></div>
            <div class="span3 flyIn">
                <div class="people">
                    <img class="team-thumb img-circle" src="<?=base_url()?>assets/assetsLanding/img/team/img-3.jpg" alt="" />
                    <h3>KEMAHASISWAAN</h3>
                    <p>
                        Kemahasiswaan ILKOM yang dapat memverivikasi nilai ekstra Mahasiswa setelah mengikuti kegiatan ekstra
                    </p>
                </div>
            </div>
            <div class="span1"></div>
            <div class="span3 flyIn">
                <div class="people text-center">
                    <img class="team-thumb img-circle" src="<?=base_url()?>assets/assetsLanding/img/team/img-4.jpg" alt="" />
                    <h3>MAHASISWA</h3>
                    <p>
                        Mahasiswa ILKOM dari semua Program Studi yang dapat mengikuti kegiatan ekstra dari tiap ORMAWA
                    </p>
                </div>
            </div>

        </div>
    </div>
    <!-- /.container -->
</section>
<!-- end section: team -->

<!-- section: services -->
<section id="services" class="section orange">
    <div class="container">
        <h4>Alur SIPENTRA</h4>
        <!-- Four columns -->
        <div class="row">
            <div class="span3 animated-fast flyIn">
                <div class="service-box">
                    <img src="<?=base_url()?>assets/assetsLanding/img/icons/laptop.png" alt="" />
                    <h2>Lihat Data Nilai Ekstra</h2>
                    <p>
                        Tampilan data nilai ekstra dari mahasiswa Program Ilmu Komputer.
                    </p>
                </div>
            </div>
            <div class="span3 animated flyIn">
                <div class="service-box">
                    <img src="<?=base_url()?>assets/assetsLanding/img/icons/lab.png" alt="" />
                    <h2>Input Data Nilai Ekstra</h2>
                    <p>
                        Input data keikutsertaan kegiatan ekstra oleh Mahasiswa.
                    </p>
                </div>
            </div>
            <div class="span3 animated-fast flyIn">
                <div class="service-box">
                    <img src="<?=base_url()?>assets/assetsLanding/img/icons/camera.png" alt="" />
                    <h2>Edit Data Nilai Ekstra</h2>
                    <p>
                        Verifikasi data keikutsertaan dengan dokumen fisik oleh Kemahasiswaan.
                    </p>
                </div>
            </div>
            <div class="span3 animated-slow flyIn">
                <div class="service-box">
                    <img src="<?=base_url()?>assets/assetsLanding/img/icons/basket.png" alt="" />
                    <h2>Validasi Data Nilai Ekstra</h2>
                    <p>
                        Validasi data nilai ekstra yang diinputkan oleh Admin Ormawa.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end section: services -->

<!-- spacer section -->
<section class="spacer bg3">
    <div class="container">
        <div class="row">
            <div class="span10 aligncenter flyLeft">
                <blockquote class="large" style="color: #111111">
                    Ing Ngarsa Sung Tuladha. Ing Madya Mangun Karsa. Tut Wuri Handayani
                    <cite>"Ki Hadjar Dewantara"</cite>
                </blockquote>
            </div>
            <div class="span2 aligncenter flyRight">
                <i class="icon-book icon-10x" style="color: #111111"></i>
            </div>
        </div>
    </div>
</section>

<!--Modal Login-->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="transaksi">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel2">Login Form</h4>
            </div>
            <div class="modal-body">
                <div>
                    <form class="form-horizontal form-label-left input_mask" method="post" action="<?=base_url('Landing/login')?>">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <div class="field your-name form-group">
                                <label for="username">NIM / NIP / ORMAWA</label>
                                <input style="width: 80%" required type="text" class="form-control" id="username" name="username" placeholder="NIM / NIP / ORMAWA">
                                <div class="validation"></div>
                            </div>
                            <br>
                            <div class="field your-password form-group">
                                <label for="password">Password</label>
                                <input style="width: 80%" required type="password" class="form-control" id="password" name="password" placeholder="Password">
                                <div class="validation"></div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="submitbarang" type="submit" class="btn btn-lg btn-success">Login</button>
            </div>
            </form>
        </div>
    </div>
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="span6 offset3">
                <p class="copyright">
                    &copy; SIPENTRA. All rights reserved.
                <div class="credits">
                    Sistem Informasi Penilaian Ekstra by Program Ilmu Komputer
                </div>
                </p>
            </div>
        </div>
    </div>
    <!-- ./container -->
</footer>

<a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bgdark icon-2x"></i></a>

<script src="<?=base_url()?>assets/assetsLanding/js/jquery.js"></script>
<script src="<?=base_url()?>assets/assetsLanding/js/jquery.scrollTo.js"></script>
<script src="<?=base_url()?>assets/assetsLanding/js/jquery.nav.js"></script>
<script src="<?=base_url()?>assets/assetsLanding/js/jquery.localScroll.js"></script>
<script src="<?=base_url()?>assets/assetsLanding/js/bootstrap.js"></script>
<script src="<?=base_url()?>assets/assetsLanding/js/jquery.prettyPhoto.js"></script>
<script src="<?=base_url()?>assets/assetsLanding/js/isotope.js"></script>
<script src="<?=base_url()?>assets/assetsLanding/js/jquery.flexslider.js"></script>
<script src="<?=base_url()?>assets/assetsLanding/js/inview.js"></script>
<script src="<?=base_url()?>assets/assetsLanding/js/animate.js"></script>
<script src="<?=base_url()?>assets/assetsLanding/js/custom.js"></script>
<script src="<?=base_url()?>assets/assetsLanding/contactform/contactform.js"></script>

</body>

</html>