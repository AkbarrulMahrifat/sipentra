<!--Akhir Halaman-->
</div>
</section>

<!-- Jquery Core Js -->
<script src="<?=base_url()?>assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="<?=base_url()?>assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="<?=base_url()?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="<?=base_url()?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?=base_url()?>assets/plugins/node-waves/waves.js"></script>

<!-- ChartJs -->
<script src="<?=base_url()?>assets/plugins/chartjs/Chart.bundle.js"></script>

<!-- Autosize Plugin Js -->
<script src="<?=base_url()?>assets/plugins/autosize/autosize.js"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="<?=base_url()?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>

<!-- Moment Plugin Js -->
<script src="<?=base_url()?>assets/plugins/momentjs/moment.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="<?=base_url()?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<!-- Custom Js -->
<script src="<?=base_url()?>assets/js/admin.js"></script>
<script src="<?=base_url()?>assets/js/pages/forms/basic-form-elements.js"></script>
<script src="<?=base_url()?>assets/js/pages/tables/jquery-datatable.js"></script>
<script src="<?=base_url()?>assets/js/pages/ui/notifications.js"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="<?=base_url()?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js}"></script>

<!-- Demo Js -->
<script src="<?=base_url()?>assets/js/demo.js"></script>


<script type="text/javascript">
    $(document).ready(function(){

        $('#date-format').bootstrapMaterialDatePicker
        ({
            format: 'DD-MM-YYYY'
        });
        $('#date-fr').bootstrapMaterialDatePicker
        ({
            format: 'DD-MM-YYYY',
            lang: 'fr',
            weekStart: 1,
            cancelText: 'ANNULER',
            nowButton: true,
            switchOnClick: true
        });

        $('.editkegiatan').on('click', function(){
            $('#id_kegiatan').val($(this).data('idkegiatan'));
            $('#nama_kegiatan').val($(this).data('namakegiatan'));
            $('#tgl_mulai').val($(this).data('tglmulai'));
            $('#tgl_selesai').val($(this).data('tglselesai'));
            $('#bentuk_kegiatan').val($(this).data('bentukkegiatan')).change();
            $('#ukuran_kegiatan').val($(this).data('ukurankegiatan')).change();
            $('#bidang_kegiatan').val($(this).data('bidangkegiatan')).change();
            $('#deskripsi_kegiatan').val($(this).data('deskripsikegiatan'));
            $('#old_proposal').val($(this).data('oldproposal'));
            $('#editkegiatan').modal('show');
        });

        $('.editnilai').on('click', function(){
            $('#id_nilai').val($(this).data('idnilai'));
            $('#id_mahasiswa').val($(this).data('idmahasiswa'));
            $('#nama_kegiatan').val($(this).data('namakegiatan')).change();
            $('#id_jabatan').val($(this).data('idjabatan')).change();
            $('#editnilai').modal('show');
        });

        $('.hapus').on("click", function (e) {
            e.preventDefault();

            var choice = confirm($(this).attr('data-confirm'));

            if (choice) {
                window.location.href = $(this).attr('href');
            }
        });

        $('.validasi').on("click", function (e) {
            e.preventDefault();

            var choice = confirm($(this).attr('data-confirm'));

            if (choice) {
                window.location.href = $(this).attr('href');
            }
        });
    });
</script>

</body>

</html>