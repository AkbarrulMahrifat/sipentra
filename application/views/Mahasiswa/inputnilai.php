<div class="col-sm-6">
    <div class="card">
        <div class="header">
            <h2>
                Tambah Data Nilai Ekstra
            </h2>
        </div>
        <div class="body">
            <form action="<?=site_url('Nilai/tambahnilaiekstra')?>" method="post">

                <div class="form-group">
                    <div class="form-line">
                        <label for="nama_kegiatan">Nama Kegiatan</label>
                        <input class="form-control" value="<?php echo $kegiatan['nama_kegiatan'] ?>" readonly>
                        <input name="nama_kegiatan" id="nama_kegiatan" class="form-control" value="<?php echo $kegiatan['id_kegiatan'] ?>" type="hidden">
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-line">
                        <label for="jabatan">Jabatan</label>
                        <select name="jabatan" id="jabatan" type="text" class="form-control" data-live-search="true" required>
                            <option value="">-- Pilih Satu Jabatan --</option>
                            <optgroup label="Bentuk Kegiatan 1 : Seminar, Workshop, Simposium, Kuliah Umum, Diskusi Panel, Talkshow, Pelatihan">
                                <option value="3">Ketua Panitia</option>
                                <option value="4">Anggota Panitia</option>
                                <option value="7">Pemateri</option>
                                <option value="6">Peserta</option>
                            </optgroup>
                            <optgroup label="Bentuk Kegiatan 2 : Pagelaran, Pameran">
                                <option value="3">Ketua Panitia</option>
                                <option value="4">Anggota Panitia</option>
                                <option value="5">Penampil / Talent / Guide</option>
                            </optgroup>
                            <optgroup label="Bentuk Kegiatan 3 : Startup, Pengurus, Kegiatan Insidentil, Produksi, Penulisan Karya Ilmiah Publikasi Ilmiah">
                                <option value="1">Ketua</option>
                                <option value="2">Anggota</option>
                            </optgroup>
                            <optgroup label="Bentuk Kegiatan 4 : Lomba">
                                <option value="3">Ketua Panitia</option>
                                <option value="4">Anggota Panitia</option>
                                <option value="6">Peserta</option>
                            </optgroup>
                            <optgroup label="Bentuk Kegiatan 5 : Asisten Penelitian Dosen, Asisten Laboratorium">
                                <option value="2">Anggota</option>
                            </optgroup>
                        </select>
                    </div>
                </div>

                <br>
                <button type="submit" class="btn btn-primary m-t-15 waves-effect">SIMPAN</button>
            </form>
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="card">
        <div class="header">
            <h2>
                Keterangan :
            </h2>
        </div>
        <div class="body">
            <ol>
                <li>Pengurus Struktural merupakan salah satu dari Sekretaris, Bendahara, Perlenglakapan, Hubungan Masyarakat, Peneliti dan Pengembang, dan atau yang setingkat, berada satu tingkat dibawah Ketua Umum</li>
                <li>Pengurus Fungsional merupakan salah satu dari Kepala Bidang atau Divisi dibawah pengurus struktural seperti Koordinator Bidang</li>
                <li>Panitia Struktural merupakan salah satu dari Sekretaris, Bendahara, Perlengkapan, Hubungan Masyarakat, dan atau yang setingkat, dan berada satu tingkat dibawah Ketua Panitia </li>
                <li>Panitia Fungsional merupakan selain dai Panitia Struktural yang telah dimaksud pada poin sebelumnya</li>

            </ol>
        </div>
    </div>
</div>
