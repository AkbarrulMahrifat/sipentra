<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Detail Kegiatan Ekstra <?=$detail['id_kegiatan']?>
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>ID Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detail['id_kegiatan']?> </td>
                        </tr>
                        <tr>
                            <td>Nama Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detail['nama_kegiatan']?> </td>
                        </tr>
                        <tr>
                            <td>Tanggal Mulai</td>
                            <td> : </td>
                            <td> <?=$detail['tgl_mulai']?> </td>
                        </tr>
                        <tr>
                            <td>Tanggal Selesai</td>
                            <td> : </td>
                            <td> <?=$detail['tgl_selesai']?> </td>
                        </tr>
                        <tr>
                            <td>Bentuk Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detail['nama_bentuk']?> </td>
                        </tr>
                        <tr>
                            <td>Ukuran Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detail['nama_ukuran']?> </td>
                        </tr>
                        <tr>
                            <td>Bidang Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detail['nama_bidang']?> </td>
                        </tr>
                        <tr>
                            <td>Deskripsi Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detail['deskripsi']?> </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <a class="btn btn-sm btn-success" href="<?=site_url('Nilai/inputnilaiekstra/'.$detail['id_kegiatan'])?>"><i class="material-icons">input</i><span>Ikuti Kegiatan</span></a>
                <a class="btn btn-sm btn-warning" href="<?=site_url('Kegiatan')?>"><i class="material-icons">keyboard_return</i><span>Kembali</span></a>
            </div>
        </div>
    </div>
</div>