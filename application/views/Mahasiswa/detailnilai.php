<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Detail Nilai Ekstra
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>ID Nilai</td>
                            <td> : </td>
                            <td> <?=$detailnilai['id_nilai']?> </td>
                        </tr>
                        <tr>
                            <td>Nama Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detailnilai['nama_kegiatan']?> </td>
                        </tr>
                        <tr>
                            <td>Tanggal Mulai</td>
                            <td> : </td>
                            <td> <?=$detailnilai['tgl_mulai']?> </td>
                        </tr>
                        <tr>
                            <td>Tanggal Selesai</td>
                            <td> : </td>
                            <td> <?=$detailnilai['tgl_selesai']?> </td>
                        </tr>
                        <tr>
                            <td>Nilai Bentuk Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detailnilai['nilai_bentuk']?> </td>
                        </tr>
                        <tr>
                            <td>Nilai Ukuran Kegiatan</td>
                            <td> : </td>
                            <td> <?=$detailnilai['nilai_ukuran']?> </td>
                        </tr>
                        <tr>
                            <td>Nilai Jabatan</td>
                            <td> : </td>
                            <td> <?=$detailnilai['nilai_jabatan']?> </td>
                        </tr>
                        <tr>
                            <td>Total Nilai</td>
                            <td> : </td>
                            <td> <?=$detailnilai['total_nilai']?> </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <a class="btn btn-sm btn-primary" href="<?=site_url('Nilai')?>"><i class="material-icons">arrow_back</i><span>Kembali</span></a>
            </div>
        </div>
    </div>
</div>