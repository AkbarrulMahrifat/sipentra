<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Daftar Kegiatan Ekstra
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama Kegiatan</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Bentuk</th>
                            <th>Ukuran</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($kegiatan as $k) { ?>
                        <tr>
                            <td><?=$k->id_kegiatan?></td>
                            <td><?=$k->nama_kegiatan?></td>
                            <td><?=$k->tgl_mulai?></td>
                            <td><?=$k->tgl_selesai?></td>
                            <td><?=$k->nama_bentuk?></td>
                            <td><?=$k->nama_ukuran?></td>
                            <td style="min-width: 80px; text-align: center">
                                <a class="btn btn-xs btn-primary" href="<?=site_url('Kegiatan/detailkegiatan/'.$k->id_kegiatan)?>"><i class="material-icons">search</i></a>
                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>