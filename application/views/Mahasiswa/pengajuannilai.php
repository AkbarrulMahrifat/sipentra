<div class="col-sm-6">
    <div class="card">
        <div class="header">
            <h2>
                Pengajuan Nilai Ekstra
            </h2>
        </div>
        <div class="body">
            <form action="<?=site_url('Nilai/tambah_pengajuan_nilai')?>" method="post">

                <div class="form-group">
                    <div class="form-line">
                        <label for="nama_kegiatan">Nama Kegiatan</label>
                        <input name="nama_kegiatan" id="nama_kegiatan" class="form-control" type="text" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-line">
                        <label>Tanggal Mulai</label>
                        <input name="tanggal_mulai" type="text" class="form-control datepicker" placeholder="Tanggal Kegiatan" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-line">
                        <label>Tanggal Selesai</label>
                        <input name="tanggal_selesai" type="text" class="form-control datepicker" placeholder="Tanggal Kegiatan" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-line">
                        <label for="bentuk_kegiatan">Bentuk Kegiatan</label>
                        <select name="bentuk" class="form-control" data-live-search="true" required>
                            <option value="">-- Bentuk Kegiatan --</option>
                            <?php foreach ($bentuk as $b) { ?>
                                <option value="<?=$b->id_bentuk?>"><?=$b->nama_bentuk?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-line">
                        <label for="ukuran_kegiatan">Ukuran Kegiatan</label>
                        <select name="ukuran" class="form-control" data-live-search="true" required>
                            <option value="">-- Ukuran Kegiatan --</option>
                            <?php foreach ($ukuran as $u) { ?>
                                <option value="<?=$u->id_ukuran?>"><?=$u->nama_ukuran?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-line">
                        <label for="bidang_kegiatan">Bidang Kegiatan</label>
                        <select name="bidang" class="form-control" data-live-search="true" required>
                            <option value="">-- Bidang Kegiatan --</option>
                            <?php foreach ($bidang as $b) { ?>
                                <option value="<?=$b->id_bidang?>"><?=$b->nama_bidang?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-line">
                        <label for="jabatan">Jabatan</label>
                        <select name="jabatan" id="jabatan" type="text" class="form-control" data-live-search="true" required>
                            <option value="">-- Pilih Satu Jabatan --</option>
                            <optgroup label="Bentuk Kegiatan 1 : Seminar, Workshop, Simposium, Kuliah Umum, Diskusi Panel, Talkshow, Pelatihan">
                                <option value="3">Ketua Panitia</option>
                                <option value="4">Anggota Panitia</option>
                                <option value="7">Pemateri</option>
                                <option value="6">Peserta</option>
                            </optgroup>
                            <optgroup label="Bentuk Kegiatan 2 : Pagelaran, Pameran">
                                <option value="3">Ketua Panitia</option>
                                <option value="4">Anggota Panitia</option>
                                <option value="5">Penampil / Talent / Guide</option>
                            </optgroup>
                            <optgroup label="Bentuk Kegiatan 3 : Startup, Pengurus, Kegiatan Insidentil, Produksi, Penulisan Karya Ilmiah Publikasi Ilmiah">
                                <option value="1">Ketua</option>
                                <option value="2">Anggota</option>
                            </optgroup>
                            <optgroup label="Bentuk Kegiatan 4 : Lomba">
                                <option value="3">Ketua Panitia</option>
                                <option value="4">Anggota Panitia</option>
                                <option value="6">Peserta</option>
                            </optgroup>
                            <optgroup label="Bentuk Kegiatan 5 : Asisten Penelitian Dosen, Asisten Laboratorium">
                                <option value="2">Anggota</option>
                            </optgroup>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-line">
                        <label for="ormawa">Ormawa Yang Diikuti</label>
                        <select name="ormawa" class="form-control" data-live-search="true" required>
                            <option value="">-- Ormawa Yang Diikuti --</option>
                            <?php foreach ($ormawa as $o) { ?>
                                <option value="<?=$o->username?>"><?=$o->nama?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <br>
                <button type="submit" class="btn btn-primary m-t-15 waves-effect">KIRIM</button>
            </form>
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="card">
        <div class="header">
            <h2>
                Keterangan :
            </h2>
        </div>
        <div class="body">
            <ol>
                <li>Pengurus Struktural merupakan salah satu dari Sekretaris, Bendahara, Perlenglakapan, Hubungan Masyarakat, Peneliti dan Pengembang, dan atau yang setingkat, berada satu tingkat dibawah Ketua Umum</li>
                <li>Pengurus Fungsional merupakan salah satu dari Kepala Bidang atau Divisi dibawah pengurus struktural seperti Koordinator Bidang</li>
                <li>Panitia Struktural merupakan salah satu dari Sekretaris, Bendahara, Perlengkapan, Hubungan Masyarakat, dan atau yang setingkat, dan berada satu tingkat dibawah Ketua Panitia </li>
                <li>Panitia Fungsional merupakan selain dai Panitia Struktural yang telah dimaksud pada poin sebelumnya</li>

            </ol>
        </div>
    </div>
</div>
