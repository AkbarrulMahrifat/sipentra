<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>SIPENTRA - <?=$this->uri->segment(1) ?></title>
    <!-- Favicon-->
    <link rel="icon" href="<?=base_url()?>assets/assetsLanding/img/unej.png" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?=base_url()?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?=base_url()?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?=base_url()?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="<?=base_url()?>assets/plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?=base_url()?>assets/css/themes/all-themes.css" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?=base_url()?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet"/>

    <!-- Wait Me Css -->
    <link href="<?=base_url()?>assets/plugins/waitme/waitMe.css" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="<?=base_url()?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>

    <!-- Jquery DataTable Plugin CSS -->
    <link href="<?=base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
</head>

<body class="theme-red">

<div class="bg-white" style="color: #111111; width: 21cm; height: 29.7cm; padding-top: 1cm; padding-right: 1cm; padding-bottom: 1cm; padding-left: 1cm">
    <div>
        <div>
            <div class="header">
                <h2>
                    Rekap Nilai Ekstra
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table>
                        <tbody>
                        <tr>
                            <td style="min-width: 100px"><p>Nama</p></td>
                            <td style="min-width: 50px; text-align: center"> <p> : </p> </td>
                            <td> <p> <?=$mahasiswa['nama']?> </p> </td>
                        </tr>
                        <tr>
                            <td style="min-width: 100px"><p>NIM</p></td>
                            <td style="min-width: 50px;text-align: center"> <p> : </p> </td>
                            <td> <p><?=$mahasiswa['nim']?></p> </td>
                        </tr>
                        <tr>
                            <td style="min-width: 100px"><p>Program Studi</p></td>
                            <td style="min-width: 50px; text-align: center"> <p> : </p> </td>
                            <td> <p><?=$mahasiswa['prodi']?></p> </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover dataTable">
                        <thead>
                        <tr>
                            <th style="width: 5px">No</th>
                            <th>Nama Kegiatan</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Jabatan</th>
                            <th>Nilai</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; foreach ($nilai as $n) { ?>
                            <tr>
                                <td><?=$no?></td>
                                <td><?=$n->nama_kegiatan?></td>
                                <td><?=$n->tgl_mulai?></td>
                                <td><?=$n->tgl_selesai?></td>
                                <td><?=$n->jabatan?></td>
                                <td><?=$n->total_nilai?></td>
                            </tr>
                            <?php $no++; } ?>
                        <tr>
                            <td colspan="5" class="text-center">Total Nilai</td>
                            <td>
                                <?php
                                $totalnilai = 0;
                                $total = $totalnilai += $n->total_nilai;
                                echo $total;
                                ?>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Jquery Core Js -->
<script src="<?=base_url()?>assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="<?=base_url()?>assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="<?=base_url()?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="<?=base_url()?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?=base_url()?>assets/plugins/node-waves/waves.js"></script>

<!-- ChartJs -->
<script src="<?=base_url()?>assets/plugins/chartjs/Chart.bundle.js"></script>

<!-- Autosize Plugin Js -->
<script src="<?=base_url()?>assets/plugins/autosize/autosize.js"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="<?=base_url()?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>

<!-- Moment Plugin Js -->
<script src="<?=base_url()?>assets/plugins/momentjs/moment.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="<?=base_url()?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<!-- Custom Js -->
<script src="<?=base_url()?>assets/js/admin.js"></script>
<script src="<?=base_url()?>assets/js/pages/forms/basic-form-elements.js"></script>
<script src="<?=base_url()?>assets/js/pages/tables/jquery-datatable.js"></script>
<script src="<?=base_url()?>assets/js/pages/ui/notifications.js"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="<?=base_url()?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="<?=base_url()?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js}"></script>

<!-- Demo Js -->
<script src="<?=base_url()?>assets/js/demo.js"></script>


<script type="text/javascript">
    $(document).ready(function(){

        $('#date-format').bootstrapMaterialDatePicker
        ({
            format: 'DD-MM-YYYY'
        });
        $('#date-fr').bootstrapMaterialDatePicker
        ({
            format: 'DD-MM-YYYY',
            lang: 'fr',
            weekStart: 1,
            cancelText: 'ANNULER',
            nowButton: true,
            switchOnClick: true
        });

        $('.editkegiatan').on('click', function(){
            $('#id_kegiatan').val($(this).data('idkegiatan'));
            $('#nama_kegiatan').val($(this).data('namakegiatan'));
            $('#tgl_kegiatan').val($(this).data('tglkegiatan'));
            $('#bentuk_kegiatan').val($(this).data('bentukkegiatan')).change();
            $('#ukuran_kegiatan').val($(this).data('ukurankegiatan')).change();
            $('#deskripsi_kegiatan').val($(this).data('deskripsikegiatan'));
            $('#old_proposal').val($(this).data('oldproposal'));
            $('#editkegiatan').modal('show');
        });

        $('.hapus').on("click", function (e) {
            e.preventDefault();

            var choice = confirm($(this).attr('data-confirm'));

            if (choice) {
                window.location.href = $(this).attr('href');
            }
        });
    });
</script>

</body>

</html>