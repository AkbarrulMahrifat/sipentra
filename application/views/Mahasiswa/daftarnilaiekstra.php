<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Daftar Nilai Ekstra
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama Kegiatan</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Bentuk</th>
                            <th>Ukuran</th>
                            <th>Jabatan</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($nilai as $n) { ?>
                            <tr>
                                <td><?=$n->id_nilai?></td>
                                <td><?=$n->nama_kegiatan?></td>
                                <td><?=$n->tgl_mulai?></td>
                                <td><?=$n->tgl_selesai?></td>
                                <td><?=$n->bentuk_kegiatan?></td>
                                <td><?=$n->ukuran_kegiatan?></td>
                                <td><?=$n->jabatan?></td>
                                <td style="text-align: center">
                                    <?php if ($n->status == 0){?>
                                    <span class="label bg-blue">Belum Disetujui</span>
                                    <?php } ?>
                                    <?php if ($n->status == 1){?>
                                    <span class="label bg-green">Telah Disetujui</span>
                                    <?php } ?>
                                    <?php if ($n->status == 2){?>
                                        <span class="label bg-red">Ditolak</span>
                                    <?php } ?>
                                </td>
                                <td style="min-width: 80px; text-align: center">
                                    <a class="btn btn-xs btn-primary" href="<?=site_url('Nilai/detailnilai/'.$n->id_nilai)?>"><i class="material-icons">search</i></a>
                                    <?php if ($n->status == 0 && $n->label_nilai == "ORMAWA"){?>
                                        <a class="btn btn-xs btn-danger" href="<?=site_url('Nilai/editnilai/'.$n->id_nilai)?>"><i class="material-icons">edit</i></a>
                                    <?php }?>
                                    <?php if ($n->status == 0 && $n->label_nilai == "MANDIRI"){ ?>
                                        <a class="btn btn-xs btn-danger" href="<?=site_url('Nilai/editpengajuannilai/'.$n->id_nilai)?>"><i class="material-icons">edit</i></a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Modal Edit Nilai-->
<div class="modal fade" id="editnilai" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Edit Data Kegiatan Ekstra</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('Nilai/updatenilaiekstra'); ?>
                <input type="hidden" id="id_nilai" name="id_nilai">
                <input type="hidden" id="id_mahasiswa" name="id_mahasiswa">
                <div class="row clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label>Nama Kegiatan</label>
                                <select name="id_kegiatan" class="form-control" id="id_kegiatan" data-live-search="true" required>
                                    <option value="">-- Nama Kegiatan --</option>
                                    <?php foreach ($kegiatan as $k) { ?>
                                        <option value="<?=$k->id_kegiatan?>"><?=$k->nama_kegiatan?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="jabatan">Jabatan</label>
                                <select name="id_jabatan" class="form-control" id="id_jabatan" data-live-search="true" required>
                                    <option value="">-- Bentuk Kegiatan --</option>
                                    <?php foreach ($jabatan as $j) { ?>
                                        <option value="<?=$j->id_jabatan?>"><?=$j->nama_jabatan?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-link waves-effect">SIMPAN</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
            </div>
            </form>
        </div>
    </div>
</div>