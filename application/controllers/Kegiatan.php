<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 10/17/2018
 * Time: 1:48 AM
 */

class Kegiatan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('role')){
            redirect("/");
        }
        $this->load->model('M_login');
        $this->load->model('M_kegiatan');
    }

    public function index(){
        if($this->session->userdata('role') == 1){
            $id_ormawa = $this->session->userdata('username');
            $data['bentuk'] = $this->M_kegiatan->getbentukkegiatan()->result();
            $data['ukuran'] = $this->M_kegiatan->getukurankegiatan()->result();
            $data['bidang'] = $this->M_kegiatan->getbidangkegiatan()->result();
            $data['kegiatan'] = $this->M_kegiatan->getkegiatanormawa($id_ormawa)->result();
            $this->load->view('header', $data);
            $this->load->view('Ormawa/daftarkegiatan');
            $this->load->view('footer');
        }
        elseif($this->session->userdata('role') == 2){
            $data['ormawa'] =$this->M_kegiatan->getormawa()->result();
            $data['kegiatan'] =$this->M_kegiatan->getkegiatan()->result();
            $this->load->view('header', $data);
            $this->load->view('Kemahasiswaan/daftarormawa');
            $this->load->view('footer');
        }
        elseif($this->session->userdata('role') == 3){
            $data['kegiatan'] = $this->M_kegiatan->getkegiatan()->result();
            $this->load->view('header', $data);
            $this->load->view('Mahasiswa/daftarkegiatan');
            $this->load->view('footer');
        }
        else{
            redirect(site_url('/'));
        }
    }

    public function rekapkegiatan($id_ormawa){
        $data['kegiatan'] = $this->M_kegiatan->getkegiatanormawa($id_ormawa)->result();
        $this->load->view('header', $data);
        $this->load->view('Kemahasiswaan/rekapkegiatan');
        $this->load->view('footer');
    }

    public function rekapkegiatanmahasiswa($id_kegiatan){
        $kegiatan = $this->M_kegiatan->getdetailkegiatan($id_kegiatan)->row_array();
        $data['mahasiswa'] = $this->M_kegiatan->getkegiatanmahasiswa($kegiatan['nama_kegiatan'])->result();
        $this->load->view('header', $data);
        $this->load->view('Kemahasiswaan/rekapkegiatanmahasiswa');
        $this->load->view('footer');
    }

    public function inputkegiatan(){
        if($this->session->userdata('role') != 1){
            redirect("/");
        }
        $data['bentuk'] = $this->M_kegiatan->getbentukkegiatan()->result();
        $data['ukuran'] = $this->M_kegiatan->getukurankegiatan()->result();
        $data['bidang'] = $this->M_kegiatan->getbidangkegiatan()->result();
        $this->load->view('header', $data);
        $this->load->view('Ormawa/inputkegiatan');
        $this->load->view('footer');
    }

    public function tambahkegiatan(){
        //konfigurasi upload file
        $config['upload_path']          = './upload/';
        $config['allowed_types']        = 'pdf|doc|docx';
        $config['overwrite']			= true;

        $this->load->library('upload', $config);

        $nama_kegiatan = $this->input->post('nama');
        $tgl_mulai = $this->input->post('tanggal_mulai');
        $tgl_selesai = $this->input->post('tanggal_selesai');
        $bentuk_kegiatan = $this->input->post('bentuk');
        $ukuran_kegiatan = $this->input->post('ukuran');
        $bidang_kegiatan = $this->input->post('bidang');
        $deskripsi = $this->input->post('deskripsi');
        $id_ormawa = $this->session->userdata('username');

        if ($this->upload->do_upload('proposal')) { //jika ada file yang diupload
            $data = array(
                'nama_kegiatan' => $nama_kegiatan,
                'tgl_mulai' => $tgl_mulai,
                'tgl_selesai' => $tgl_selesai,
                'id_bentuk' => $bentuk_kegiatan,
                'id_ukuran' => $ukuran_kegiatan,
                'id_bidang' => $bidang_kegiatan,
                'id_ormawa' => $id_ormawa,
                'proposal' => $this->upload->data('file_name'),
                'deskripsi' => $deskripsi,
            );
            $this->M_kegiatan->tambahkegiatan($data);
            $this->session->set_flashdata('success','Data berhasil ditambah!');
        } else { //jika tidak ada file yang diupload
            $data = array(
                'nama_kegiatan' => $nama_kegiatan,
                'tgl_mulai' => $tgl_mulai,
                'tgl_selesai' => $tgl_selesai,
                'id_bentuk' => $bentuk_kegiatan,
                'id_ukuran' => $ukuran_kegiatan,
                'id_bidang' => $bidang_kegiatan,
                'id_ormawa' => $id_ormawa,
                'proposal' => NULL,
                'deskripsi' => $deskripsi,
            );
            $this->M_kegiatan->tambahkegiatan($data);
            $this->session->set_flashdata('success','Data berhasil ditambah! Tidak ada file proposal yang diupload, silahkan edit kegiatan.');
        }

        redirect('Kegiatan');
    }

    public function updatekegiatan(){
        //konfigurasi upload file
        $config['upload_path']          = './upload/';
        $config['allowed_types']        = 'pdf|doc|docx';
        $config['max_size']             = 0;

        $this->load->library('upload', $config);

        $id_kegiatan = $this->input->post('id');
        $nama_kegiatan = $this->input->post('nama');
        $tgl_mulai = $this->input->post('tanggal_mulai');
        $tgl_selesai = $this->input->post('tanggal_selesai');
        $bentuk_kegiatan = $this->input->post('bentuk');
        $ukuran_kegiatan = $this->input->post('ukuran');
        $bidang_kegiatan = $this->input->post('bidang');
        $old_proposal = $this->input->post('old_proposal');
        $deskripsi = $this->input->post('deskripsi');
        $id_ormawa = $this->session->userdata('username');

        if ($this->upload->do_upload('proposal')) { //jika ada file yang diupload
            $data = array(
                'nama_kegiatan' => $nama_kegiatan,
                'tgl_mulai' => $tgl_mulai,
                'tgl_selesai' => $tgl_selesai,
                'id_bentuk' => $bentuk_kegiatan,
                'id_ukuran' => $ukuran_kegiatan,
                'id_bidang' => $bidang_kegiatan,
                'id_ormawa' => $id_ormawa,
                'proposal' => $this->upload->data('file_name'),
                'deskripsi' => $deskripsi,
            );
            $this->M_kegiatan->updatekegiatan($data, $id_kegiatan);
            unlink('./upload/'.$old_proposal);
        } else { //jika tidak ada file yang diupload
            $data = array(
                'nama_kegiatan' => $nama_kegiatan,
                'tgl_mulai' => $tgl_mulai,
                'tgl_selesai' => $tgl_selesai,
                'id_bentuk' => $bentuk_kegiatan,
                'id_ukuran' => $ukuran_kegiatan,
                'id_bidang' => $bidang_kegiatan,
                'id_ormawa' => $id_ormawa,
                'proposal' => $old_proposal,
                'deskripsi' => $deskripsi,
            );
            $this->M_kegiatan->updatekegiatan($data, $id_kegiatan);
        }

        redirect('Kegiatan');
    }

    public function deletekegiatan($id_kegiatan){
        //ini pake soft delete, jadi data akan hilang dari tampilan tapi tetep kesimpen di database
        $data = array(
            'is_delete' => 1,
        );
        $this->M_kegiatan->deletekegiatan($data, $id_kegiatan);
        redirect('Kegiatan');
    }

    public function detailkegiatan($id_kegiatan){
        $data['detail'] = $this->M_kegiatan->getdetailkegiatan($id_kegiatan)->row_array();
        if($this->session->userdata('role') == 1){
            $this->load->view('header', $data);
            $this->load->view('Ormawa/detailkegiatan');
            $this->load->view('footer');
        }
        elseif($this->session->userdata('role') == 2){
            $this->load->view('header', $data);
            $this->load->view('Kemahasiswaan/detailkegiatan');
            $this->load->view('footer');
        }
        elseif($this->session->userdata('role') == 3){
            $this->load->view('header', $data);
            $this->load->view('Mahasiswa/detailkegiatan');
            $this->load->view('footer');
        }
        else{
            redirect(site_url('/'));
        }

    }

    public function downloadproposal($id_kegiatan){
        $file = $this->M_kegiatan->getdetailkegiatan($id_kegiatan)->row()->proposal;
        if ($file == NULL){
            redirect('Kegiatan/detailkegiatan/'.$id_kegiatan);
        } else {
            force_download('upload/'.$file,NULL);
        }

    }
}