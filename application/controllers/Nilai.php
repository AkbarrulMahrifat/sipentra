<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 10/17/2018
 * Time: 1:53 AM
 */

class Nilai extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('role')){
            redirect("/");
        }
        $this->load->model('M_login');
        $this->load->model('M_nilai');
        $this->load->model('M_kegiatan');
    }

    public function index()
    {
        if ($this->session->userdata('role') == 1) {
            $id_ormawa = $this->session->userdata('username');
            $data['nilai'] = $this->M_nilai->getnilaiperormawa($id_ormawa)->result();
            $this->load->view('header', $data);
            $this->load->view('Ormawa/validasinilaiekstra');
            $this->load->view('footer');
        } elseif ($this->session->userdata('role') == 2) {
            $data['nilai'] = $this->M_nilai->getmahasiswa()->result();
            $this->load->view('header',$data);
            $this->load->view('Kemahasiswaan/daftarnilaiekstra');
            $this->load->view('footer');
        } elseif ($this->session->userdata('role') == 3) {
            $id_mahasiswa = $this->session->userdata('username');
            $data['nilai'] = $this->M_nilai->getnilaimahasiswa($id_mahasiswa)->result();
            $data['rata2nilai'] = $this->M_nilai->totalnilai($id_mahasiswa)->row_array();
            $data['kegiatan'] = $this->M_kegiatan->getkegiatan()->result();
            $data['jabatan'] = $this->M_nilai->getjabatan()->result();
            $this->load->view('header', $data);
            $this->load->view('Mahasiswa/daftarnilaiekstra');
            $this->load->view('footer');
        } else {
            redirect(site_url('/'));
        }
    }

    public function inputnilaiekstra($id_kegiatan){
        $data['kegiatan'] = $this->M_kegiatan->getdetailkegiatan($id_kegiatan)->row_array();
        $data['jabatan'] = $this->M_nilai->getjabatan()->result();
        $this->load->view('header', $data);
        $this->load->view('Mahasiswa/inputnilai.php');
        $this->load->view('footer');
    }

    public function tambahnilaiekstra(){
        $id_mahasiswa = $this->session->userdata('username');
        $id_kegiatan = $this->input->post('nama_kegiatan');
        $id_jabatan = $this->input->post('jabatan');

        $kegiatan = $this->M_nilai->getnilaikegiatan($id_kegiatan)->row_array();
        $jabatan = $this->M_nilai->getnilaijabatan($id_jabatan)->row_array();
        $nilai = $kegiatan['nilai_bentuk'] + $kegiatan['nilai_ukuran'] + $jabatan['nilai_jabatan'];

        $data = array(
            'id_mahasiswa' => $id_mahasiswa,
            'id_ormawa' => $kegiatan['id_ormawa'],
            'nama_kegiatan' => $kegiatan['nama_kegiatan'],
            'bentuk_kegiatan' => $kegiatan['nama_bentuk'],
            'ukuran_kegiatan' => $kegiatan['nama_ukuran'],
            'bidang_kegiatan' => $kegiatan['nama_bidang'],
            'tgl_mulai' => $kegiatan['tgl_mulai'],
            'tgl_selesai' => $kegiatan['tgl_selesai'],
            'jabatan' => $jabatan['nama_jabatan'],
            'total_nilai' => $nilai,
            'label_nilai' => "ORMAWA",
            'status' => 0,
        );

        $this->M_nilai->tambahnilaiekstra($data);
        $this->session->set_flashdata('success','Data berhasil ditambah!');
        redirect('Nilai');
    }

    public function editnilai($id){
        $data['nilai'] = $this->M_nilai->detailnilai($id)->row_array();
        $data['kegiatan'] = $this->M_kegiatan->getkegiatan()->result();
        $this->load->view('header', $data);
        $this->load->view('Mahasiswa/editnilai');
        $this->load->view('footer');
    }
    public function updatenilaiekstra(){
        $id_nilai = $this->input->post('id_nilai');
        $id_kegiatan = $this->input->post('id_kegiatan');
        $id_jabatan = $this->input->post('jabatan');

        $kegiatan = $this->M_nilai->getnilaikegiatan($id_kegiatan)->row_array();
        $jabatan = $this->M_nilai->getnilaijabatan($id_jabatan)->row_array();
        $nilai = $kegiatan['nilai_bentuk'] + $kegiatan['nilai_ukuran'] + $jabatan['nilai_jabatan'];

        $data = array(
            'id_ormawa' => $kegiatan['id_ormawa'],
            'nama_kegiatan' => $kegiatan['nama_kegiatan'],
            'bentuk_kegiatan' => $kegiatan['nama_bentuk'],
            'ukuran_kegiatan' => $kegiatan['nama_ukuran'],
            'bidang_kegiatan' => $kegiatan['nama_bidang'],
            'tgl_mulai' => $kegiatan['tgl_mulai'],
            'tgl_selesai' => $kegiatan['tgl_selesai'],
            'jabatan' => $jabatan['nama_jabatan'],
            'total_nilai' => $nilai,
            'status' => 0,
        );

        $status = $this->M_nilai->updatenilaiekstra($data, $id_nilai);
        var_dump($status);
        $this->session->set_flashdata('success','Data berhasil diubah!');
        redirect('Nilai');
    }

    public function pengajuannilaiekstra(){
        $data['bentuk'] = $this->M_kegiatan->getbentukkegiatan()->result();
        $data['ukuran'] = $this->M_kegiatan->getukurankegiatan()->result();
        $data['bidang'] = $this->M_kegiatan->getbidangkegiatan()->result();
        $data['jabatan'] = $this->M_nilai->getjabatan()->result();
        $data['ormawa'] = $this->M_nilai->getormawa()->result();
        $this->load->view('header', $data);
        $this->load->view('Mahasiswa/pengajuannilai.php');
        $this->load->view('footer');
    }

    public function tambah_pengajuan_nilai(){
        $id_mahasiswa = $this->session->userdata('username');
        $nama_kegiatan = $this->input->post('nama_kegiatan');
        $tanggal_mulai = $this->input->post('tanggal_mulai');
        $tanggal_selesai = $this->input->post('tanggal_selesai');
        $id_bentuk = $this->input->post('bentuk');
        $id_ukuran = $this->input->post('ukuran');
        $id_bidang = $this->input->post('bidang');
        $id_jabatan = $this->input->post('jabatan');
        $ormawa = $this->input->post('ormawa');

        $bentuk = $this->M_nilai->getnilaibentuk($id_bentuk)->row_array();
        $ukuran = $this->M_nilai->getnilaiukuran($id_ukuran)->row_array();
        $bidang = $this->M_nilai->getnilaibidang($id_bidang)->row_array();
        $jabatan = $this->M_nilai->getnilaijabatan($id_jabatan)->row_array();
        $nilai = $bentuk['nilai_bentuk'] + $ukuran['nilai_ukuran'] + $jabatan['nilai_jabatan'];

        $data = array(
            'id_mahasiswa' => $id_mahasiswa,
            'nama_kegiatan' => $nama_kegiatan,
            'bentuk_kegiatan' => $bentuk['nama_bentuk'],
            'ukuran_kegiatan' => $ukuran['nama_ukuran'],
            'bidang_kegiatan' => $bidang['nama_bidang'],
            'id_ormawa' => $ormawa,
            'tgl_mulai' => $tanggal_mulai,
            'tgl_selesai' => $tanggal_selesai,
            'jabatan' => $jabatan['nama_jabatan'],
            'total_nilai' => $nilai,
            'label_nilai' => "MANDIRI",
            'status' => 0,
        );

        $this->M_nilai->tambahnilaiekstra($data);
        $this->session->set_flashdata('success','Data berhasil ditambah!');
        redirect('Nilai');
    }

    public function detailnilai($id_nilai){
        $data['detailnilai'] = $this->M_nilai->detailnilai($id_nilai)->row_array();
        if ($this->session->userdata('role') == 1){
            $this->load->view('header', $data);
            $this->load->view('Ormawa/detailnilai');
            $this->load->view('footer');
        } elseif ($this->session->userdata('role') == 2){
            $this->load->view('header', $data);
            $this->load->view('Kemahasiswaan/detailnilai');
            $this->load->view('footer');
        } elseif ($this->session->userdata('role') == 3){
            $this->load->view('header', $data);
            $this->load->view('Mahasiswa/detailnilai');
            $this->load->view('footer');
        }
    }

    public function rekapnilaiekstra(){
        $id_mahasiswa = $this->session->userdata('username');
        $data['mahasiswa'] = $this->M_login->getdatamahasiswa($id_mahasiswa)->row_array();
        $data['nilai'] = $this->M_nilai->getrekapnilai($id_mahasiswa)->result();
        $this->load->view('header', $data);
        $this->load->view('Mahasiswa/rekapnilaiekstra');
        $this->load->view('footer');
    }

    public function rekapnilaimahasiswa($id_mahasiswa){
        $data['mahasiswa'] = $this->M_login->getdatamahasiswa($id_mahasiswa)->row_array();
        $data['nilai'] = $this->M_nilai->getrekapnilai($id_mahasiswa)->result();
        $this->load->view('header', $data);
        $this->load->view('Kemahasiswaan/rekapnilaimahasiswa');
        $this->load->view('footer');
    }

    public function validasinilai($id_nilai, $val){
        $data = array(
            'status' => $val,
        );
        $this->M_nilai->updatenilaiekstra($data, $id_nilai);
        redirect('Nilai');
    }
}