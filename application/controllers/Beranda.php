<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 10/17/2018
 * Time: 1:52 AM
 */

class Beranda extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('role')){
            redirect("/");
        }
//        $this->load->model('M_ormawa');
    }

    public function index(){
        if($this->session->userdata('role') == 1){
            $this->load->view('header');
            $this->load->view('Ormawa/beranda');
            $this->load->view('footer');
        }elseif($this->session->userdata('role') == 2){
            $this->load->view('header');
            $this->load->view('Kemahasiswaan/beranda');
            $this->load->view('footer');
        }elseif($this->session->userdata('role') == 3){
            $this->load->view('header');
            $this->load->view('Mahasiswa/beranda');
            $this->load->view('footer');
        }
        else{
            redirect(site_url('/'));
        }
    }
}