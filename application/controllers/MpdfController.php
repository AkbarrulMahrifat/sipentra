<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 10/31/2018
 * Time: 2:44 AM
 */

class MpdfController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_login');
        $this->load->model('M_nilai');
        $this->load->model('M_kegiatan');
    }

    public function printPDF()
    {
        $nim_mahasiswa = $this->session->userdata('username');
        $data['mahasiswa'] = $this->M_login->getdatamahasiswa($nim_mahasiswa)->row_array();
        $data['nilai'] = $this->M_nilai->getnilaimahasiswa($nim_mahasiswa)->result();
        $data['rata2nilai'] = $this->M_nilai->totalnilai($nim_mahasiswa)->row_array();

        $mpdf = new \Mpdf\Mpdf();
        $print = $this->load->view('Mahasiswa/page_prints', $data, TRUE);
        $mpdf->WriteHTML($print);
        $mpdf->Output("Nilai_Ekstra_".$nim_mahasiswa.".pdf", 'I');
    }

    //buat liat view printnya
    public function viewPDF(){
        $id_mahasiswa = $this->M_login->getidmahasiswa()->row()->id_mahasiswa;
        $data['mahasiswa'] = $this->M_login->getdatamahasiswa()->row_array();
        $data['nilai'] = $this->M_nilai->getnilaimahasiswa($id_mahasiswa)->result();
        $data['rata2nilai'] = $this->M_nilai->totalnilai($id_mahasiswa)->row_array();
        $this->load->view('Mahasiswa/page_prints', $data);
    }
}