<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 10/16/2018
 * Time: 11:23 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_login');
    }

    public function index(){
        if($this->session->userdata('role')){
            redirect(site_url().'Beranda');
        }
        else{
            $this->load->view('landing.php');
        }

    }

    public function login(){
        $u = $this->input->post('username');
        $p = $this->input->post('password');

        $b = $this->M_login->login($u,$p);
        if($b){
            $this->session->set_flashdata('success','Login Berhasil !');
            redirect(site_url().'Beranda');
        }
        else{
            $this->session->set_flashdata('error','Username atau password salah!');
            redirect('/');
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }
}