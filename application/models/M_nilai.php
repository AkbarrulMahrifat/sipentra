<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 10/27/2018
 * Time: 12:39 AM
 */

class M_nilai extends CI_Model
{
    public function getjabatan(){
        return $this->db->get('jabatan_kegiatan');
    }

    public function getormawa(){
        return $this->db->get_where('user', array('role' => 1));
    }

    public function getmahasiswa() {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('role', '3');
        return $this->db->get();
    }

    public function getnilaiperormawa($id_ormawa) {
        $this->db->select('*');
        $this->db->from('nilai_ekstra');
        $this->db->join('user', 'user.username = nilai_ekstra.id_mahasiswa', 'inner');
        $this->db->where('nilai_ekstra.id_ormawa', $id_ormawa);
        return $this->db->get();
    }

    public function getnilaimahasiswa($id_mahasiswa) {
        $this->db->select('*');
        $this->db->from('nilai_ekstra');
        $this->db->join('user', 'user.username = nilai_ekstra.id_mahasiswa', 'inner');
        $this->db->where('nilai_ekstra.id_mahasiswa', $id_mahasiswa);
        return $this->db->get();
    }

    public function getrekapnilai($id_mahasiswa) {
        $this->db->select('*');
        $this->db->from('nilai_ekstra');
        $this->db->join('user', 'user.username = nilai_ekstra.id_mahasiswa', 'inner');
//        $this->db->join('kegiatan', 'kegiatan.id_kegiatan = nilai_ekstra.id_kegiatan', 'inner');
//        $this->db->join('bentuk_kegiatan', 'bentuk_kegiatan.id_bentuk = kegiatan.id_bentuk', 'inner');
//        $this->db->join('ukuran_kegiatan', 'ukuran_kegiatan.id_ukuran = kegiatan.id_ukuran', 'inner');
//        $this->db->join('jabatan_kegiatan', 'jabatan_kegiatan.id_jabatan = nilai_ekstra.jabatan', 'inner');
        $this->db->where('nilai_ekstra.id_mahasiswa', $id_mahasiswa);
        $this->db->where('status', 1);
        return $this->db->get();
    }

    public function totalnilai($id_mahasiswa) {
        $this->db->select_avg('total_nilai');
        $this->db->from('nilai_ekstra');
        $this->db->where('id_mahasiswa', $id_mahasiswa);
        $this->db->where('status', 0);
        return $this->db->get();
    }

    public function getnilaikegiatan($id_kegiatan){
        $this->db->select('*');
        $this->db->from('kegiatan');
        $this->db->join('bentuk_kegiatan', 'bentuk_kegiatan.id_bentuk = kegiatan.id_bentuk', 'inner');
        $this->db->join('ukuran_kegiatan', 'ukuran_kegiatan.id_ukuran = kegiatan.id_ukuran', 'inner');
        $this->db->join('bidang_kegiatan', 'bidang_kegiatan.id_bidang = kegiatan.id_bidang', 'inner');
        $this->db->where('id_kegiatan', $id_kegiatan);
        return $this->db->get();
    }

    public function getnilaijabatan($id_jabatan){
        $this->db->select('*');
        $this->db->where('id_jabatan', $id_jabatan);
        return $this->db->get('jabatan_kegiatan');
    }

    public function tambahnilaiekstra($data){
        $this->db->insert('nilai_ekstra', $data);
    }

    public function updatenilaiekstra($data, $id_nilai){
        $this->db->where('id_nilai', $id_nilai);
        $this->db->update('nilai_ekstra', $data);
    }

    public function detailnilai($id_nilai){
        $this->db->select('*');
        $this->db->join('bentuk_kegiatan', 'bentuk_kegiatan.nama_bentuk = nilai_ekstra.bentuk_kegiatan', 'inner');
        $this->db->join('ukuran_kegiatan', 'ukuran_kegiatan.nama_ukuran = nilai_ekstra.ukuran_kegiatan', 'inner');
        $this->db->join('jabatan_kegiatan', 'jabatan_kegiatan.nama_jabatan = nilai_ekstra.jabatan', 'inner');
        $this->db->where('id_nilai', $id_nilai);
        return $this->db->get('nilai_ekstra');
    }

    public function getnilaibentuk($id_bentuk){
        return $this->db->get_where('bentuk_kegiatan', array('id_bentuk' => $id_bentuk));
    }

    public function getnilaiukuran($id_ukuran){
        return $this->db->get_where('ukuran_kegiatan', array('id_ukuran' => $id_ukuran));
    }

    public function getnilaibidang($id_bidang){
        return $this->db->get_where('bidang_kegiatan', array('id_bidang' => $id_bidang));
    }
}