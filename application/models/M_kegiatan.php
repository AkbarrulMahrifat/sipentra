<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 10/24/2018
 * Time: 12:16 PM
 */

class M_kegiatan extends CI_Model
{
    public function getbentukkegiatan() {
        return $this->db->get('bentuk_kegiatan');
    }

    public function getukurankegiatan() {
        return $this->db->get('ukuran_kegiatan');
    }

    public function getbidangkegiatan() {
        return $this->db->get('bidang_kegiatan');
    }

    public function getormawa(){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('role', 1);
        return $this->db->get();
    }

    public function getkegiatan() {
        $this->db->select('*');
        $this->db->from('kegiatan');
        $this->db->join('bentuk_kegiatan', 'bentuk_kegiatan.id_bentuk = kegiatan.id_bentuk', 'inner');
        $this->db->join('ukuran_kegiatan', 'ukuran_kegiatan.id_ukuran = kegiatan.id_ukuran', 'inner');
        $this->db->where('is_delete', 0);
        return $this->db->get();
    }

    public function getkegiatanormawa($id_ormawa) {
        $this->db->select('*');
        $this->db->from('kegiatan');
        $this->db->join('bentuk_kegiatan', 'bentuk_kegiatan.id_bentuk = kegiatan.id_bentuk', 'inner');
        $this->db->join('ukuran_kegiatan', 'ukuran_kegiatan.id_ukuran = kegiatan.id_ukuran', 'inner');
        $this->db->where('id_ormawa', $id_ormawa);
        $this->db->where('is_delete', 0);
        return $this->db->get();
    }

    public function getkegiatanmahasiswa($id_kegiatan){
        $this->db->select('*');
        $this->db->from('nilai_ekstra');
        $this->db->join('user', 'user.username = nilai_ekstra.id_mahasiswa', 'inner');
        $this->db->join('kegiatan', 'kegiatan.nama_kegiatan = nilai_ekstra.nama_kegiatan', 'inner');
        $this->db->join('jabatan_kegiatan', 'jabatan_kegiatan.nama_jabatan = nilai_ekstra.jabatan', 'inner');
        $this->db->where('kegiatan.nama_kegiatan', $id_kegiatan);
        return $this->db->get();
    }

    public function tambahkegiatan($data){
        $this->db->insert('kegiatan', $data);
    }

    public function updatekegiatan($data, $id_kegiatan) {
        $this->db->where('id_kegiatan', $id_kegiatan);
        $this->db->update('kegiatan', $data);
    }

    public function deletekegiatan($data, $id_kegiatan) {
        $this->db->where('id_kegiatan', $id_kegiatan);
        $this->db->update('kegiatan', $data);
    }

    public function getdetailkegiatan($id_kegiatan){
        $this->db->select('*');
        $this->db->from('kegiatan');
        $this->db->join('bentuk_kegiatan', 'bentuk_kegiatan.id_bentuk = kegiatan.id_bentuk', 'inner');
        $this->db->join('ukuran_kegiatan', 'ukuran_kegiatan.id_ukuran = kegiatan.id_ukuran', 'inner');
        $this->db->join('bidang_kegiatan', 'bidang_kegiatan.id_bidang = kegiatan.id_bidang', 'inner');
        $this->db->where('id_kegiatan', $id_kegiatan);
        return $this->db->get();
    }
}