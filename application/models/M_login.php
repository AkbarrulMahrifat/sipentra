<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 10/16/2018
 * Time: 11:36 PM
 */

class M_login extends CI_Model
{
    public function login($u,$p){
        $this->db->from('user');
        $this->db->where('username',$u);
        $this->db->where('password',$p);
        $a = $this->db->get();

        if($a->num_rows() == 1){
            $data=$a->result_array();
            $this->session->set_userdata('username',$data[0]['username']);
            $this->session->set_userdata('nama',$data[0]['nama']);
            $this->session->set_userdata('role',$data[0]['role']);

            return true;
        }
        else{
            return false;
        }
    }

    public function getidormawa(){
        $username = $this->session->userdata('username');
        $this->db->select('id_ormawa');
        $this->db->where('username', $username);
        return $this->db->get('ormawa');
    }

    public function getidmahasiswa(){
        $username = $this->session->userdata('username');
        $this->db->select('id_mahasiswa', 'nim');
        $this->db->where('username', $username);
        return $this->db->get('mahasiswa');
    }

    public function getdatamahasiswa($username){
        $this->db->select('*');
        $this->db->where('username', $username);
        return $this->db->get('mahasiswa');
    }
}